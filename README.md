# parezenc

Font encoding files used by the parez LaTeX package

The content of this repository should be placed (with caution) in the `/fonts/enc` directory
of your texmf tree or any fancy subdirectory therein.